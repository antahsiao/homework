const Koa = require('koa');
const logger = require('koa-logger');
const config = require('config');
const { koaSwagger } = require('koa2-swagger-ui');
const bodyParser = require('koa-bodyparser');
const swagger = require('./swagger');

const app = new Koa();
app.use(logger());
app.use(bodyParser());

app.use(
  koaSwagger({
    routePrefix: '/swagger', // host at /swagger instead of default /docs
    swaggerOptions: {
      url: '/swagger.json', // example path to json
    },
  }),
);

app.use(swagger.routes(), swagger.allowedMethods())

app.use(require('./src/rest/middlewares/filter'));
require('./src/routes')(app);

const server = app.listen(config.port, () => {
  console.log(`Server listening on port: ${config.port}`);
});
module.exports = server;

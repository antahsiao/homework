const currencyConversionService = require('../rest/services/currencyConversionService');

describe('test currencyConversionService function', () => {
    test('test convert', async () => {
      const payload = {
        codes: ['603625'],
      };
  
      let sourceCurrency = "TWD";
      let targetCurrency = "JPY";
      let amount = "amount";
      targetCurrency = "ABC";
      let result = currencyConversionService.convert(sourceCurrency, targetCurrency, amount);
      expect(result.message).toStrictEqual('Not Support targetCurrency: ABC');

      sourceCurrency = "ABC";
      result = currencyConversionService.convert(sourceCurrency, targetCurrency, amount);
      expect(result.message).toStrictEqual('Not Support sourceCurrency: ABC');

      sourceCurrency = "TWD";
      targetCurrency = "JPY";
      result = currencyConversionService.convert(sourceCurrency, targetCurrency, amount);
      expect(result.message).toStrictEqual('amount is not number: amount');

      amount = 100;
      result = currencyConversionService.convert(sourceCurrency, targetCurrency, amount);
      expect(result.result).toStrictEqual('366.90');

      sourceCurrency = "USD";
      targetCurrency = "JPY";
      amount = 100;
      result = currencyConversionService.convert(sourceCurrency, targetCurrency, amount);
      expect(result.result).toStrictEqual('11,180.10');      
    });
  });
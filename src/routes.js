const Router = require('koa-router');
const config = require('config');
const currencyConversionRouter = require('./rest/router/currencyConversionRouter');

module.exports = (app) => {
  const rootRouter = new Router({
    prefix: config.prefix,
  });

  // all router root

  rootRouter.use('/currency', currencyConversionRouter.routes());
  // rootRouter.use('/fund', fundRouter.routes());
  app.use(rootRouter.routes(), rootRouter.allowedMethods());
};

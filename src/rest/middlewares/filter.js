const ResFormator = require('../../utils/formator');
const normalFun = require('../../utils/normalFun');

module.exports = async (ctx, next) => {
  try {
    const { header } = ctx.request;
    if (normalFun.verifyToken(header.token) === false) {
      throw new Error('token is not right!!');
    }
    await next();
  } catch (error) {
    ctx.body = new ResFormator(new Error(error.message)).fullResponse;
  }
};



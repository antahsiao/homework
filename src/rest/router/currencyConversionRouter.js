const Router = require('koa-router');
const currencyConversionController = require('../controllers/currencyConversionController');

const router = new Router();

router.post('/convert', currencyConversionController.convert);

module.exports = router;

const currencies = {
  "TWD": {
    "TWD": 1,
    "JPY": 3.669,
    "USD": 0.03281
  },
  "JPY": {
    "TWD": 0.26956,
    "JPY": 1,
    "USD": 0.00885
  },
  "USD": {
    "TWD": 30.444,
    "JPY": 111.801,
    "USD": 1
  }
};

exports.convert = (sourceCurrency, targetCurrency, amount) => {
  const result = {success: false, message:'', result:''};
  if (!["TWD", "JPY", "USD"].includes(sourceCurrency)) {
    result.message = `Not Support sourceCurrency: ${sourceCurrency}`;
    return result;
  }
  if (!["TWD", "JPY", "USD"].includes(targetCurrency)) {
    result.message = `Not Support targetCurrency: ${targetCurrency}`;
    return result;
  }
  if (typeof amount !== "number") {
    result.message = `amount is not number: ${amount}`;
    return result;
  }
  const resultAmount = amount * currencies[sourceCurrency][targetCurrency];
  result.result = resultAmount.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
  result.success = true;
  return result;
};

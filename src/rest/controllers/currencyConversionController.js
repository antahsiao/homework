const ResFormator = require('../../utils/formator');
const currencyConversionService = require('../services/currencyConversionService');

exports.convert = async (ctx) => {
  const { body } = ctx.request;
  const { sourceCurrency, targetCurrency, amount } = body;
  try {
    const result = currencyConversionService.convert(sourceCurrency, targetCurrency, amount);
    if (result.success === true) {
      ctx.body = new ResFormator(result.result).fullResponse;
    } else {
      ctx.body = new ResFormator(new Error(result.message)).fullResponse;
    }    
  } catch (error) {
    ctx.body = new ResFormator(new Error(error.message)).fullResponse;
    return false;
  }
  return true;
};

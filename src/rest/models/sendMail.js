const nodemailer = require('nodemailer');
const config = require('config');

exports.sendMailBynodemailer = async (toEMAIL, title, body, attachments) => {
  return new Promise((resolve, reject) => {
    (async () => {
      try {
        // create reusable transporter object using the default SMTP transport
        const transporter = nodemailer.createTransport({
          host: config.mailHost,
          port: config.mailPort,
          tls: { rejectUnauthorized: false },
          auth: {
            user: config.mailUser,
            pass: config.mailPassword,
          },
        });

        const mailContent = {
          from: config.mailUser,
          to: toEMAIL,
          subject: title,
          html: body,
          attachments: attachments === null ? '' : attachments,
        };

        // send mail with defined transport object
        transporter.sendMail(mailContent, (err, info) => {
          if (err) {
            console.log(err);
            reject(error.message);
          } else {
            console.log(`Message sent: ${info.response}`);
          }
          transporter.close(); // 如果沒用,關閉連線
          resolve(info);
        });
      } catch (err) {
        reject(error.message);
      }
    })();
  });
};

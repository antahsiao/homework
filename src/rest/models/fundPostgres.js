const Pool = require('pg-pool');
const config = require('config');

exports.query = async (sqlcommand) => {
  let connection = null;
  let pool = null;
  try {
    pool = new Pool({
      user: config.fundUser, // env var: PGUSER
      database: config.fundDatabase, // env var: PGDATABASE
      password: config.fundPassword, // env var: PGPASSWORD
      host: config.fundHost, // Server hosting the postgres database
      port: config.fundPort, // env var: PGPORT
      connectionTimeoutMillis: 120 * 1000,
      timezone: 'Asia/Taipei',
    });
    connection = await pool.connect();
    const result = await connection.query(sqlcommand);
    return result;
  } catch (error) {
    console.error(error);
    throw new Error(error.message);
  } finally {
    if (connection) {
      try {
        connection.release();
        await pool.end();
      } catch (error) {
        console.error(error);
        throw new Error(error.message);
      }
    }
  }
};
